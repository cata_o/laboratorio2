.data

Menu1: .asciiz "\t Ingrese la opcion que desea realizar \n\t 1.- Insertar grafo \n\t 2.- Encontrar camino\n\t 3.-Salir\n"
texto_error: .asciiz "\n\tLA OPCION INGRESADA NO ES VALIDA..."
texto_nodos: .asciiz "\n\tIngrese el numero de nodos que desea agregar:  "
texto_adyacencia: .asciiz "\n\tIngrese la matriz de adyacencia del grafo que se desea ingresar: \n"
ejemplo_adyacencia: .asciiz "\n\t Ejemplo si desea ingresar la siguiente matriz de ayacencia para 4 nodos, \n\t\t |0013|\n\t\t|0121|\n\t\t|0013|\n\t\t|0000|\n\n\tSe debera ingresar de la siguiente forma "
end: .asciiz "\n\tEl camino minimo es de :  " 
vector: .word 0,5,7,8,0,0,0,0, 0,0,9,0,4,0,0,0, 0,0,0,8,0,8,0,0, 0,0,0,0,0,6,11,0 ,0,0,5,0,0,3,0,1, 0,0,0,0,0,0,0,2 ,0,0,0,0,0,9,0,10 ,0,0,0,0,0,0,0,0
op: .word 0

.text
j main						# salto a la etiqueta main

Print:				# funcion que permite imprimir el resultado del camino minimo sin retorno ya que es la funcion final
add $t7, $zero, $zero		# variable en donde se guardara el resultado de la cantidad de nodos
mientras3:			# etiqueta para realizar el ciclo
beq $sp, $a3, end_print		# Condicion para terminar el ciclo si stack pointer se encuentra en la posicionoriginal	
lw $t2, 0($sp)			# Se carga en t2 la posicion1 del stack pointer
lw $t3, 4($sp)			# Se carga en t3 la posicion2 del stack pointer
add $sp, $sp, 8			# Se realiza el pop y el stack pointer aumenta en 2 palabra
addi $t7, $t7, 1		# contador de la variable final	

#li $v0, 1				# cargamos v0 con 1 para imprimir un entero
#move $a0, $t2				# movemos lo que se encuentre en t2 (resultado) a a0 para realizar la impresion del resultado
#syscall				# hacemos el llamado al sistema
#li $v0, 1				# cargamos v0 con 1 para imprimir un entero
#move $a0, $t3				# movemos lo que se encuentre en t2 (resultado) a a0 para realizar la impresion del resultado
#syscall				# hacemos el llamado al sistema
j mientras3				# genera el ciclo
end_print:				# etiqueta que nos permite salir del ciclo

la $t0, end				# se carga en t0 el labes end
					
li $v0, 4				# Se asigna en v0 el valor 4 para imprimir un string
move $a0, $t0 				# se mueve t0 a a0 para ser impreso
syscall					# llamada al sistema

li $v0, 1					# cargamos v0 con 1 para imprimir un entero
move $a0, $t7					# movemos lo que se encuentre en t2 (resultado) a a0 para realizar la impresion del resultado
syscall						# llamado al sistema
j Salir						# se sale del programa

ir_posicion:				# funcion que nos permite avanzar hasta el nodo que corresponde los contadores para ir avanzando en el vector y retorna el numero del nodo al que se dirige con el camino elegido
mientras2:				# etiqueta que nos permite hacer el ciclo
	beq $t0, 64, end_mientras2	# Condicion de borde t0 = 64 se termina el ciclo
	beq $t5, $t3, end_mientras2	# Condicion de borde t3 = t5 terminar ciclo, t5 posee el valor del nodo en donde se encuentra y t3 hasta donde tiene que ir
	beq $t1, 8, aux1		#  Conidicion de borde t1 =8, se dirige a aux1 para evitar posibles errores que se pueden generar si existe un datos importante en esa posicion
	aum1:				# etiqueta aum1 nos permite aumentar los valores tanto de la posiciones como de los argumentos
	addi $t0, $t0, 1		# t0++
	addi $a1, $a1, 4 		# se le suma una palabra 4 bytes para que al inicio del vector o array quede el siguiente dato a leer
	addi $t1, $t1, 1		# t1 ++
	j mientras2			# realiza el salto para hacer un ciclo
aux1:					# etiqueta para obtener los datos que se puedan encontrar en la condicion de borde
addi $t5, $t5, 1			# t5 ++
li $t1, 1				# t1 =1
addi $t0, $t0, 1			# t0++
addi $a1, $a1 4				# se le suma una palabra 4 bytes para que al inicio del vector o array quede el siguiente dato a leer
beq $t5, $t3, end_mientras2		# Condicion de borde t3 = t5 terminar ciclo, t5 posee el valor del nodo en donde se encuentra y t3 hasta donde tiene que ir	
j aum1					# relaiza el aum cuando no se cumple la condicion anterior mencionada	
end_mientras2:				# etiqueta que permite salir del ciclo
jr $ra					# se devuelve a donde se realizo el jal mediante el registro ra	


guarda:					# funcion guardar que recibe como parametro t2 (el valor del camino que se desea guardar) y t3 (al nodo que se dirige)
addi $sp, $sp , -8			# nos reserva 2 palabras en el stack pointer
sw $t2, 0($sp)				# en la primera palabra o posicion guardamos t2
sw $t3, 4($sp)				# en la segunda palabra o poscion guardamos t3
j aum					# se hace una especie de retorno a la parte de la funcion que ha requerido guardar 
					
compara:				# funcion compara que recibe como parametro el valor actual distinto de cero y lo compara con el que fue guardado ultimamente
bgt $t4, $t2, aum			# Condicion t4 > $t2 no se guarda 		
reemplaza:				# etiqueta que es utilizada como auxiliar para reemplazar un dato directamente 
move $t3, $t1				# se mueve el dato anterior y se reemplaza por el que se encuentra en t1
move $t2, $t4				# se mueve el dato anterior y se reemplaza por el que se encuentra en t4
beq $t3, 8, guarda			# condicion de borde que permite guardar si el nodo que se dirige es la cantidad de nodos totales
j aum					# si no se cumple lo anterior salta a aum
						
encuentra_camino:			# Funcion encuentra minimo, que recibe y utiliza como parametro un numero de nodo y retorna el valor minimo de ese nodo	
mul $t2, $t2, $zero			# t2 =0
lw $t4, 0($a1)				# t4 = a1[1]
mientras1:				# etiqueta que nos permite realizar un ciclo
	beq $t0, 64, end_mientras1	# Condicion de borde t0 = 64 se termina el ciclo	
	beq $t1, 8, aux			# Conidicion de borde t1 =8, se dirige a aux para evitar posibles errores que se pueden generar si existe un datos importante en esa posicion	
	beq $t4, $zero, aum		# Condicion que nos permite discriminar si en t4 no encuentra ni un valor	
	bne $t2, $zero, compara		# Condicion que nos permite saber si exite un valor ya guardado	
	j reemplaza			# etiqueta que nos permite reemplazar inmediatamente los datos
	
	aux:				# etiqueta aux que nos ayuda a analizar cuando existe un valor en la ultima posicion
	beq $t4, $zero, guarda		# si se llega al final de la lectura por nodo se guarda el ultimo dato guardado temporalmente
	bne $t2, $zero, compara		# si se encuentra previamente un valor guardado este va a ser comparado
	j reemplaza			# de lo contrario se reemplazara
	aum:				# etiqueta aum que nos permite aumentar hasta la ultima de posicion que en este caso es 8
	beq $t0, 64, end_mientras1	# Condicion de borde t0 = 64 se termina el ciclo	
	beq $t1, 8, end_mientras1	# condicion de borde t1 =8, se dirige a aux para evitar posibles errores que se pueden generar si existe un datos importante en esa posicion	
	addi $t0, $t0, 1		# t0++
	addi $t1, $t1, 1		# t1++
	addi $a1, $a1, 4		# se le suma una palabra 4 bytes para que al inicio del vector o array quede el siguiente dato a leer
	lw $t4, 0($a1)			# t4 = a1[0]
	j mientras1			# realiza el salto para realizar un ciclo
end_mientras1:				# etiqueta que nos permite salir del ciclo
jr $ra					# se devuelve a la instruccion que se encuentra en ra

Busca_camino:				# Funcion que busca el camino minimo sin retorno ya qu es la funcion principal y utiliza las 2 funciones mas importantes que vimos anteriormente 
	la $t0, vector			# se carga en t0 el contenido de vector previamente definidido
	move $a1, $t0			# se mueve el valor antes leido a a1 ya que sera tomado como argumento
	addi $t0, $zero, 1		# t0 = 1 (aumenta hasta valor de los nodos al cuadrado)
	addi $t1, $zero, 1		# t1 = 1 (aumenta hasta la cantidad de nodos y vuelve a partir de 1)
	addi $t5, $zero, 1		# t5 = 1 (cada vez que el anterior vuelve a 1 este registro aumenta en 1)
	add $t2, $zero, $zero		# t2 = 0 (registro utilizado para guardar temporalmente el costo del nodo)
	add $t3, $zero, $zero		# t3 = 0 (registro utilizado para guardar temporalmente )	
	add $a3, $sp, $zero		# a3 = sp (se guarda la direccion inicial del stack pointer antes de ser alterado)
	mientras:			# etiqueta para realizar el ciclo
	beq $t0, 64, end_mientras	# Condicion de borde t0 = 64 se termina el ciclo	
	jal encuentra_camino		# se llama a la funcion encuentra_camino esperando su retorno
	beq $t0, 64, end_mientras	# Condicion de borde t0 = 64 se termina el ciclo	
	li $t1, 1			# t1 = 1
	addi $t0, $t0, 1		# t0++
	addi $t5, $t5, 1		# t5++
	addi $a1, $a1, 4		# se le suma una palabra 4 bytes para que al inicio del vector o array quede el siguiente dato a leer
	jal ir_posicion			# se llama a la funcion ir_posicion esperando su retorno 
	j mientras			# salta a mientras para realizar el ciclo
	end_mientras:			# etiqueta que nos permite salir del ciclo	
	j Print				# se realiza la impresion de la funcion
					


Agrega: # funcion que nos permite agregar dinamicamente una matriz de adyacencia
la $t0, texto_nodos

li $v0, 4
move $a0, $t0 
syscall


li $v0, 5
Syscall

move $a2, $v0
mul $a3, $a2, $a2
add $t2, $zero, $zero
mul $t3, $a3, 4
move $a0, $t3
li $v0, 9
syscall
move $a1, $v0
la $t0, ejemplo_adyacencia

li $v0, 4
move $a0, $t0 
syscall

la $t0, texto_adyacencia

li $v0, 4
move $a0, $t0 
syscall

repite:
	beq $a3, $t2, end_repite
	
	li $v0, 5
	Syscall 
	move $t4, $v0
	sw $t4, 0($a1)
	addi $a1, $a1, 4
	addi $t2, $t2, 1
	j repite
end_repite:
j Salir	
				
Salir:						# etiqueta salir
li $v0, 10					# cargamos v0 con 10, para terminar la ejecucion
syscall						# hacemos el llamado al sistema



main:	# funcion principal

la $t0, Menu1

li $v0, 4
move $a0, $t0 
syscall

li $v0, 5
Syscall

beq $v0, 1, Agrega
agrega1:
beq $v0, 2, Busca_camino
beq $v0, 3, Salir

la $t0, texto_error
li $v0, 4
move $a0, $t0   
syscall