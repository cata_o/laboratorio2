.data

Menu1: .asciiz "\t Ingrese la opcion que desea realizar \n\t 1.- Insertar grafo \n\t 2.- Encontrar camino hasta nodo\n\t 3.-Salir\n"
texto_error: .asciiz "\n\tLA OPCION INGRESADA NO ES VALIDA..."
texto_nodos: .asciiz "\n\tIngrese el numero de nodos que desea agregar:  "
texto_adyacencia: .asciiz "\n\tIngrese la matriz de adyacencia del grafo que se desea ingresar: \n"
ejemplo_adyacencia: .asciiz "\n\t Ejemplo si desea ingresar la siguiente matriz de ayacencia para 4 nodos, \n\t\t |0013|\n\t\t|0121|\n\t\t|0013|\n\t\t|0000|\n\n\tSe debera ingresar de la siguiente forma "
vector: .word 0,5,7,8,0,0,0,0, 0,0,9,0,4,0,0,0, 0,0,0,8,0,8,0,0, 0,0,0,0,0,6,11,0 ,0,0,5,0,0,3,0,1, 0,0,0,0,0,0,0,2 ,0,0,0,0,0,9,0,10 ,1,0,0,0,0,0,0,0


.text
j main						# salto a la etiqueta main


Agrega:
la $t0, texto_nodos

li $v0, 4
move $a0, $t0 
syscall


li $v0, 5
Syscall

move $a2, $v0
mul $a3, $a2, $a2
add $t2, $zero, $zero
mul $t3, $a3, 4
move $a0, $t3
li $v0, 9
syscall
move $a1, $v0
la $t0, ejemplo_adyacencia

li $v0, 4
move $a0, $t0 
syscall

la $t0, texto_adyacencia

li $v0, 4
move $a0, $t0 
syscall

repite:
	beq $a3, $t2, end_repite
	
	li $v0, 5
	Syscall 
	move $t4, $v0
	sw $t4, 0($a1)
	addi $a1, $a1, 4
	addi $t2, $t2, 1
	j repite
end_repite:
j Salir

				#	

sig_nivel:				# funcion que no opera correctamente
mientras2:				#
	beq $t0, 64, end_mientras2	#
	beq $t5, $t3, end_mientras2	#
	beq $t1, 8, aux1		#
	aum1:				#	
	addi $t0, $t0, 1		#
	addi $a1, $a1, 4		#	
	addi $t1, $t1, 1		#
	j mientras2			#
aux1:					#
addi $t5, $t5, 1			#
li $t1, 1				#
addi $t0, $t0, 1			#	
addi $a1, $a1 4				#
beq $t5, $t3, end_mientras2		#
j aum1					#
end_mientras2:				#
jr $ra					#
					

guardar:				# Funcion que nos permite guardar los datos validos
addi $sp, $sp , -8			# guarda el espacio de 2 palabras
sw $t1, 0($sp)				# guarda en la primera posicion lo que se encuentra t1
sw $t4, 4($sp)				# guarda en la segunda posicion lo que se encuentra en t4
li $v0, 1					# cargamos v0 con 1 para imprimir un entero
move $a0, $t1					# movemos lo que se encuentre en t2 (resultado) a a0 para realizar la impresion del resultado
syscall						# hacemos el llamado al sistema
li $v0, 1					# cargamos v0 con 1 para imprimir un entero
move $a0, $t4					# movemos lo que se encuentre en t2 (resultado) a a0 para realizar la impresion del resultado
syscall						# hacemos el llamado al sistema
j aum						# etiqueta que hace el salto al aum	
				


encuentra_adyacentes:			# Funcion que nos permite obtner los adyacentes a un nodo en particular
mul $t2, $t2, $zero			# t2 =0
lw $t4, 0($a1)				# t4 = a1[1]		
mientras1:				# etiqueta que nos permite realizar un ciclo
	beq $t0, 64, end_mientras1	#  Condicion de borde t0 !=64
	beq $t1, 8, aux			# Condicion de borde t1 != de lo contrario se ira a la etiqueta
	beq $t4, $zero, aum		# condicion que nos permite saber si el valor que se esta leyendo es cero	
	j guardar			# guarda si las condiciones anteriores no se cumplen
	aux:				# etiqueta que nos ayuda a prevenir posibles errores cuando existen datos al final 
	addi $t5, $t5, 1		# t5 ++
	li $t1, 1			# t1 =1
	addi $t0, $t0, 1		# t0++
	addi $a1, $a1 4			# se hace avanzar la palabra sumando 4 bytes
	j guardar			# se guarda
	aum:				# etiqueta aum que nos permite aumentar todos los parametros
	addi $a1, $a1, 4		# se hace avanzar la palabra sumando 1 palabra
	addi $t0, $t0, 1		# t0++
	addi $t1, $t1, 1		# t1++
	lw $t4, 0($a1)			# t4 = a1[1]
	j mientras1			# salta a la etiqueta mientras1 para realizar el ciclo
end_mientras1:				# etiqueta que nos permite terminar el ciclo
jr $ra					# se devuelve a la instruccion contenida en ra

Busca_nodo:				# Funcion principal, que recibe el vector desde memoria o de manera dinamica mediante el teclado
	la $t0, vector			# se copia t0 a a1 
	move $a1, $t0			# a1 = vector
	addi $t0, $zero, 1		# t0 = 1
	addi $t1, $zero, 1		# t1 = 1	
	addi $t5, $zero, 1		# t5 = 1	
	add $a3, $sp, $zero		# a3 = sp
	mientras:			# etiqueta que nos permite realizar el ciclo
	beq $t0, 64, end_mientras	# condicion de borde t0 = 64 termina el ciclo	
	jal encuentra_adyacentes	# se llama a encuentra_adyacentes esperando retorno	
	beq $t0, 64, end_mientras	# condicion de borde t0 =64 termina el ciclo	 
	li $t1, 1			# t1 = 1	 
	addi $t0, $t0, 1		# t0 ++
	addi $t5, $t5, 1		# t5 ++
	addi $a1, $a1, 4		# se avanza el vector en una palabra
	#jal sig_nivel			#
	j mientras			# se salta a la etiqueta mientras para hacer el ciclo
	end_mientras:			#
j Salir					#
Salir:						# etiqueta salir
li $v0, 10					# cargamos v0 con 10, para terminar la ejecucion
syscall						# hacemos el llamado al sistema




main:				# funcion principal

la $t0, Menu1

li $v0, 4
move $a0, $t0 
syscall

li $v0, 5
Syscall

beq $v0, 1, Agrega
beq $v0, 2, Busca_nodo
beq $v0, 3, Salir

la $t0, texto_error
li $v0, 4
move $a0, $t0   
syscall
